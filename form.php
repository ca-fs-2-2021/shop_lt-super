<?php

?>

<div class="container">
    <div>

        <h1>Add product</h1>
        <form action="#" class="form-group">
            <label for="name">Product Name:</label>
            <input type="text" class="form-control" name="name" id="name">

            <label for="description">Product Description:</label>
            <textarea rows="4" cols="50" class="form-control" name="description"></textarea>

            <label for="sku">Unique Code:</label>
            <input type="text" class="form-control" name="sku">

            <label for="price">Price:</label>
            <input type="number" class="form-control" name="price">

            <label for="specialprice">Special Price:</label>
            <input type="number" class="form-control" name="specialprice">

            <label for="cost">Cost:</label>
            <input type="number" class="form-control" name="cost">

            <label for="quantity">Quantity:</label>
            <input type="number" class="form-control" name="quantity" placeholder="quantity">

        </form>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>